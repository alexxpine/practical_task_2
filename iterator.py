class EvenNumbers:
    def __init__(self, start, end):
        self._current = start
        self._end = end

    def __iter__(self):
        return self

    def __next__(self):
        while self._current < self._end:
            if self._current % 2 == 0:
                num = self._current
                self._current += 1
                return num
            else:
                self._current += 1
        raise StopIteration
