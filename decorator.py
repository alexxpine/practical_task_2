from functools import wraps
from time import time


def speed_test(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        start_time = time()
        result = fn(*args, **kwargs)
        end_time = time()
        print('--------------------------------------')
        print(f'Executing {fn.__name__}')
        print(f'Time Elapsed: {end_time - start_time}')
        return result
    return wrapper
