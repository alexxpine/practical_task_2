def fib():
    a = 0
    b = 1
    start_index = 0
    while True:
        yield a
        start_index += 1
        a, b = b, a + b
