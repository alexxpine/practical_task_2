from iterator import EvenNumbers
from generator import fib
from decorator import speed_test
from context_manager import SpeedTest

import math

# Iterator
"""EvenNumbers() takes 2 arguments 
to set start(included) and end(excluded) points of the range
Everytime you invoke next() method on the object of EvenNumbers, 
it returns the next even number in the range
"""
even_numbers = EvenNumbers(0, 10)

print('Even numbers:')
for elem in even_numbers:
    print(elem)
print('')

# Generator
"""Fibonacci Series:
Everytime you invoke next() method 
on the fib generator object,
it returns the next item of the Fibonacci series
"""

fib = fib()

print('Fibonacci series:')
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print('')

# Decorator
"""Calculates the time difference when 
the function execution started and ended
"""


@speed_test
def sum_nums_gen():
    return sum(x for x in range(50000000))


@speed_test
def sum_nums_list():
    return sum([x for x in range(50000000)])


print('Compare the time elapsed for calculating the sum ' +
      'using object generator and list comprehension:')
sum_nums_gen()
sum_nums_list()


# Context manager
"""Calculates the time difference when 
the function execution started and ended
"""

with SpeedTest() as st_factorial:
    math.factorial(500000)

print('')
print(f'It took {st_factorial()}s to calculate factorial ' +
      'of the first 500000 numbers')
