from time import time


class SpeedTest:
    def __enter__(self):
        self._start_time = time()
        return lambda: self._end_time - self._start_time

    def __exit__(self, exc_type, exc_value, exc_tb):
        self._end_time = time()
